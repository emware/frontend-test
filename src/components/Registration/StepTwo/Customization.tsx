import React, { useEffect, useState } from "react";
import { RegistrationCustomization } from "../../../types/registration";

interface Props {
    onComplete: (customization: RegistrationCustomization) => void,
    disabled: boolean,
    isCompleted: boolean,
}

export const Customization: React.FunctionComponent<Props> = ({onComplete, disabled, isCompleted}) => {
    const [customization, setCustomization] = useState<RegistrationCustomization>({});
    const [customBadgeRequired, setCustomBadgeRequired] = useState<boolean|undefined>(undefined);
    const [accommodationRequired, setAccommodationRequired] = useState<boolean|undefined>(undefined);

    useEffect(() => {
        if (customBadgeRequired === undefined || accommodationRequired === undefined) {
            return;
        }

        if (customBadgeRequired && !customization.companyNameOnBadge) {
            return;
        }

        if (accommodationRequired && !customization.accommodationDetails) {
            return;
        }

        onComplete(customization);
    }, [customization, customBadgeRequired, accommodationRequired, onComplete]);

    return (
        <fieldset className="step" disabled={disabled}>
            <legend>Step 2</legend>
            <div>
                <p>
                    Would you like your company name on your badges?
                </p>
                <input type="radio"
                       name="company_name_toggle_group"
                       onChange={() => setCustomBadgeRequired(previousState => true)}
                />
                <label htmlFor="company_name_toggle_on">Yes</label>
                <input type="radio"
                       name="company_name_toggle_group"
                       onChange={() => {
                           setCustomBadgeRequired(previousState => false);
                           setCustomization(previousCustomization => {
                               const {companyNameOnBadge, ...customization} = previousCustomization;
                               return customization;
                           });
                       }}
                />
                <label htmlFor="company_name_toggle_off">No</label>
            </div>
            <div id="company_name_wrap" hidden={!customBadgeRequired}>
                <label htmlFor="company_name">
                    Company Name:
                </label>
                <input type="text"
                       id="company_name"
                       onBlur={event => {
                           const companyName = event.target.value;
                           setCustomization(previousCustomization => ({
                               ...previousCustomization,
                               companyNameOnBadge: companyName
                           }));
                       }}/>
            </div>
            <div>
                <p>
                    Will anyone in your group require special accommodations?
                </p>
                <input type="radio" id="special_accommodations_toggle_on"
                       name="special_accommodations_toggle"
                       onChange={() => setAccommodationRequired(previousState => true)}
                />
                <label htmlFor="special_accommodations_toggle_on">Yes</label>
                <input type="radio" id="special_accommodations_toggle_off"
                       name="special_accommodations_toggle"
                       onChange={() => {
                           setAccommodationRequired(previousState => false);
                           setCustomization(previousCustomization => {
                               const {accommodationDetails, ...customization} = previousCustomization;
                               return customization;
                           });
                       }}
                />
                <label htmlFor="special_accommodations_toggle_off">No</label>
            </div>
            <div id="special_accommodations_wrap" hidden={!accommodationRequired}>
                <label htmlFor="special_accomodations_text">
                    Please explain below:
                </label>
                <textarea rows={10}
                          cols={10}
                          id="special_accomodations_text"
                          onBlur={event => {
                              const accommodationDetails = event.target.value;
                              setCustomization(previousCustomization => ({
                                  ...previousCustomization,
                                  accommodationDetails: accommodationDetails
                              }));
                          }}/>
            </div>
        </fieldset>
    )
};