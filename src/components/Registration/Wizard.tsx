import React, { FormEvent, useEffect } from 'react';
import { Attendees } from './StepOne/Attendees';
import { Customization } from './StepTwo/Customization';
import { CompleteRegistration } from './StepThree/CompleteRegistration';
import { useMachine } from "@xstate/react/lib";
import wizardMachine from "../../stateMachine/wizardMachine";
import { AttendeeList } from "../../types/registration";

export const Wizard: React.FunctionComponent = () => {
    const [current, send] = useMachine(wizardMachine, {
        actions: {
            sideEffect: () => console.log('sideEffect')
        }
    });

    const setAttendeeListHandler = (list: AttendeeList): void => {
        send("SET_ATTENDEE_LIST", {list: list});
    };

    const setCustomizationHandler = (customization: object): void => {
        send("SET_RESERVATION_CUSTOMIZATION", {customization: customization});
    };

    const onSubmitHandler = (event: FormEvent) => {
        event.preventDefault();
        console.log(current.context);
        send("SUBMIT");
    };

    useEffect(() => {
        if(current.matches("completed")) send("RESTART");
    }, [current.value]);

    const wizardContent = current.matches("completed")
        ? <h2>Registration completed</h2>
        :
        <form id="Wizard" action="#" method="post" onSubmit={onSubmitHandler}>
            <Attendees
                disabled={!current.matches("stepOne")}
                onComplete={setAttendeeListHandler}
                isCompleted={current.matches("stepTwo") || current.matches("stepThree")}
                initialAttendees={current.context.attendees}
            />
            <Customization
                disabled={!current.matches("stepTwo")}
                onComplete={setCustomizationHandler}
                isCompleted={current.matches("stepThree")}
            />
            <CompleteRegistration
                disabled={!current.matches("stepThree")}
            />
        </form>;

    return <>{wizardContent}</>
};
