import React, { useState } from "react";

interface Props {
    disabled: boolean
}

export const CompleteRegistration: React.FunctionComponent<Props> = ({disabled}) => {
    const [ready, setReady] = useState<boolean>(false);

    return (
        <fieldset className="step" disabled={disabled}>
            <legend>Step 3</legend>
            <label htmlFor="rock">
                Are you ready to rock?
            </label>
            <input type="checkbox" id="rock" checked={ready} onChange={event => {
                const isReady = event.target.checked;
                setReady(previousReady => isReady);
            }}/>
            <input type="submit" id="submit_button" value="Complete Registration" disabled={!ready}/>
        </fieldset>
    )
};