import React from "react";
import { Attendee } from "../../../types/registration";

interface Props {
    index: number,
    addAttendee: (index: number, attendee: Attendee) => void
}

export const AttendeeInput: React.FunctionComponent<Props> = ({index, addAttendee}) => {
    return <>
        <div id={`attendee_${index}_wrap`}>
            <label htmlFor="name_attendee_1">
                Attendee {index + 1} Name:
            </label>
            <input type="text" id={`name_attendee_${index}`} onBlur={event => addAttendee(index, {name: event.target.value})}/>
        </div>
    </>
};