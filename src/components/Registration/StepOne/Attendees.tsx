import React, { useEffect, useState } from "react";
import { AttendeeInput } from "./AttendeeInput";
import { Attendee, AttendeeList } from "../../../types/registration";

interface Props {
    onComplete: (list: AttendeeList) => void,
    disabled: boolean,
    isCompleted: boolean,
    initialAttendees: AttendeeList,
}

export const Attendees: React.FunctionComponent<Props> = ({onComplete, disabled, isCompleted, initialAttendees}) => {
    const [attendeesAmount, setAttendeesAmount] = useState<number>(0);
    const [attendees, setAttendees] = useState<AttendeeList>(initialAttendees);

    const addAttendeeHandler = (index: number, attendee: Attendee): void =>
        setAttendees(previousAttendeeList => {
            const clone = [...previousAttendeeList];
            clone[index] = attendee;

            if (clone.length === attendeesAmount) {
                onComplete(clone);
            }
            return clone;
        });

    const setAttendeesAmountHandler = (amount: number) => {
        setAttendees(attendees.slice(0, amount));
        setAttendeesAmount(amount);
    };

    const attendeesInputList = [...Array(attendeesAmount).keys()].map(attendeeIndex => (
        <AttendeeInput index={attendeeIndex} addAttendee={addAttendeeHandler}/>
    ));

    const attendeeListTitle = attendeesAmount ? <h3>Please provide full names:</h3> : "";

    return (
        <fieldset className="step" disabled={disabled}>
            <legend>Step 1</legend>
            <label htmlFor="num_attendees">
                How many people will be attending?
            </label>
            <select value={attendeesAmount} onChange={event => {
                const amount = event.target.value;
                setAttendeesAmountHandler(parseInt(amount));
            }} id="num_attendees">
                <option value="0">Please Choose</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            <div>
                {attendeeListTitle}
                {attendeesInputList}
                <span className={isCompleted ? "completed" : ""}></span>
            </div>
        </fieldset>
    )
};
