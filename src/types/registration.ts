export interface Attendee {
    name: string,
}

export type AttendeeList = Array<Attendee>;

export type RegistrationCustomization = {
    companyNameOnBadge?: string,
    accommodationDetails?: string,
}