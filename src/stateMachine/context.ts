import { AttendeeList, RegistrationCustomization } from "../types/registration";

export type WizardContext = {
    attendees: AttendeeList;
} & RegistrationCustomization;

const getInitialContext = (): WizardContext => ({
    attendees: [],
});

export default getInitialContext;