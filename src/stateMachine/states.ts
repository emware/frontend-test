export type WizardState = {
    states: {
        stepOne: {},
        stepTwo: {},
        stepThree: {},
        completed: {},
    }
}