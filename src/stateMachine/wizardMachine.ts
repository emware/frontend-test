import { assign, Machine } from 'xstate';
import getInitialContext, { WizardContext } from "./context";
import { WizardState } from "./states";
import { AttendeeList } from "../types/registration";

export type WizardEvent =
    | { type: 'SET_ATTENDEE_LIST'; list: AttendeeList }
    | { type: 'SET_RESERVATION_CUSTOMIZATION'; customization: object }
    | { type: 'SUBMIT'; }
    | { type: 'RESTART'; };

const setAttendeesList = (context: WizardContext, event: any) => {
    return {
        ...context,
        attendees: event.list,
    };
};

const setReservationCustomization = (context: WizardContext, event: any) => {
    return {
        ...context,
        ...event.customization,
    };
};

const resetContext = (context: WizardContext, event: any) => {
    return getInitialContext();
};

export default Machine<WizardContext, WizardState, WizardEvent>({
    key: "wizard",
    initial: "stepOne",
    context: getInitialContext(),
    states: {
        stepOne: {
            entry: assign(resetContext),
            on: {
                SET_ATTENDEE_LIST: {
                    target: "stepTwo",
                    actions: assign(setAttendeesList),
                },
            },
        },
        stepTwo: {
            on: {
                SET_RESERVATION_CUSTOMIZATION: {
                    target: "stepThree",
                    actions: assign(setReservationCustomization),
                },
            },
        },
        stepThree: {
            on: {SUBMIT: "completed"},
        },
        completed: {
            on: {RESTART: "stepOne"},
        },
    },
});