import React from 'react';
import './App.css';
import { Wizard } from './components/Registration/Wizard';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Seminar <span>Registration</span></h1>
      </header>
      <Wizard />
    </div>
  );
}

export default App;
