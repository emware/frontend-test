QUESTIONS

1. Which browser and which tools do you usually use to develop? (Operating system, browser, various tools).
I worked with linux, windows and OSX environment, OSX is actually my operating system of choice.
For frontend development i use Chrome as main browser combined with it's DevTool. I also use some extension like React and Redux DevTools.
As main IDE, i use PhpStorm for both PHP backend development and ES/Typescript frontend development. I Also sometimes use VSCode.
ITerm2 is constantly open, i use zsh with oh-my-zsh, other plugins and custom alias.
DataGrip for relational db interactions and administration. 
I also love to manage time with the pomodoro technique, so i use Be Focused to track pomodoros.

2. How do you optimize assets? (CSS, Js, Images).
In the past i used gulp to run task for css and js minifications. With angular and then react i moved to webpack that helps me create an optimized asset's bundle.
If i need to optimize the images size, i use tools to shrink the size before use them in my applications.

3. Why a website usually serves assets from different domains?
A different cookie-less domain (or a subdomain) that serve only static assets can reduce the request overhead given by main required domain's cookie.
Also multiple parallels request to different domains can reduce the overall response time.

4. Which new technology would you like to learn during this year?
On the frontend sid, i'd love to deeper my knowledge of the React framework along with typescript, RxJs and practice even more with functional programming paradigm.
I also like to continue to improve my DevOps knowledge that helps me deploy software faster. I want to learn more about security to become something more like a DevSecOps.

5. Write 3 methods to reduce the page’s load time.
Assets optimization, caching, compression, chunking, requests parallelization, use of a CDN.

6. Do you use any CSS preprocessor? If yes, describe what do you like about preprocessors and what you don’t.
I've used SASS in the past. I'd like to keep the CSS as simple as possibile and with preprocessor is quite fast to produce a bloated code, with complex logic that can 
become hard to understand.

7. How do you include a non standard font in a website?
For include a non standard font i can use the css @font-face rule. If the font is used only for add icons it's better consider using SVG images instead. 